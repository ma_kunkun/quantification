# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
import time
import okex.spot_api as spot
import okex.futures_api as future


api_key = ''
seceret_key = ''
passphrase = ''
spotAPI = spot.SpotAPI(api_key, seceret_key, passphrase, True)
futureAPI = future.FutureAPI(api_key, seceret_key, passphrase, True)

def taobao(side,symbol,contract_id,contract_value,values,dif,times,period):
    def get_spot_price():
        while True:
            try:
                s_price = spotAPI.get_specific_ticker(symbol+'-USDT')
                s_price1 = float(s_price.get('best_ask'))
                s_price2 = float(s_price.get('best_bid'))
            except Exception as e:
                print(f'币币获取价格失败，error is {e}')
                time.sleep(3)
                continue
            if s_price1:
                break
        return [s_price1,s_price2]
    
    def get_future_price():
        while True:
            try:
                f_price = futureAPI.get_specific_ticker(symbol+'-USD-'+contract_id)
                f_price1 = float(f_price.get('best_ask'))
                f_price2 = float(f_price.get('best_bid'))
            except Exception as e:
                print(f'合约获取价格失败，error is {e}')
                time.sleep(3)
                continue
            if f_price1:
                break
        return  [f_price1,f_price2]
    
    def make_sure():
        if side == 'buy':
            diff = (get_future_price()[1] - get_spot_price()[0]) / get_spot_price()[0]
            if diff > dif :
                return True
            else:
                return False
        if side == 'sell':
            diff = (get_future_price()[0] - get_spot_price()[1]) / get_spot_price()[1]
            if diff < dif :
                return True
            else:
                return False
    def make_sure2(amount):
        if side == 'buy':
            s_balance = spotAPI.get_coin_account_info('USDT')
            b_balance = 100*times
            if float(s_balance.get('available')) > b_balance:
                return True
            else:
                return False
        if side=='sell':
            s_balance = spotAPI.get_coin_account_info(symbol)
            if float(s_balance.get('available')) > amount:
                return True
            else:
                return False 
            
    def take_order():
        if side == 'buy':
            s_amount = float('%.2f'%(100*times/get_spot_price()[0]))
            f_amount = int(100*times/contract_value)
            if make_sure2(s_amount):
                try:
                    s_order = spotAPI.take_order('market', side, symbol+'-USDT', s_amount,100*times)
                    f_order = futureAPI.take_order(symbol+'-USD-'+contract_id, 2, 1, f_amount, 1)
                except Exception as e:
                    print(f'下单失败,error is {e}')
        if side == 'sell':
            s_amount = float('%.2f'%(100*times/get_spot_price()[1]))
            f_amount =  int(100*times/contract_value)
            if make_sure2(s_amount):
                try:
                    s_order = spotAPI.take_order('market', side, symbol+'-USDT', s_amount,100*times)
                    f_order = futureAPI.take_order(symbol+'-USD-'+contract_id, 4, 1, f_amount, 1)
                except Exception as e :
                    print(f'下单失败,error is {e}')
            
    number = 0
    n = values / (100*times)
    t_time = time.strftime('%m-%d %H:%M:%S')
    print(t_time)
    print('套保交易开始建仓--' + str(n)+'次!' )
    while True:
        if make_sure():
            take_order()
            number+=1
            print(str(number))
        if number >= n:
            t_time = time.strftime('%m-%d %H:%M:%S')
            print(t_time)
            print('已经全部交易完成。')
            break
        time.sleep(period)
        

            



            
